#pragma once
#include "Farbe.h"
#include "Rot.h"


class Ampel
{
	public:
		Ampel();
		~Ampel();

		void umschalten();
		Farbe* getAnzeige();

	private:
		Farbe* anzeige = new Rot();
};

