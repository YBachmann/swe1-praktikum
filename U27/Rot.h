#pragma once
#include "Farbe.h"

class Rot :	public Farbe
{
	public:
		Rot();
		~Rot();

		Farbe* wechseln();
		void anzeigen();
};

