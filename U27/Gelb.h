#pragma once
#include "Farbe.h"

class Gelb : public Farbe
{
	public:
		Gelb();
		~Gelb();

		Farbe* wechseln();
		void anzeigen();
};

