#pragma once
#include "Farbe.h"

class Orange : public Farbe
{
	public:
		Orange();
		~Orange();

		Farbe* wechseln();
		void anzeigen();
};

