#include "Gruen.h"
#include "Gelb.h"
#include <iostream>

Gelb::Gelb()
{}

Gelb::~Gelb()
{}

Farbe* Gelb::wechseln() {
	return new Gruen();
}

void Gelb::anzeigen() {
	std::cout << "Ampel ist gelb." << std::endl;
}
