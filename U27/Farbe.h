#pragma once
class Farbe
{
public:
	Farbe();
	virtual ~Farbe();
	virtual Farbe* wechseln()= 0;
	virtual void anzeigen() = 0;
};

