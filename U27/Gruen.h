#pragma once
#include "Farbe.h"

class Gruen : public Farbe
{
	public:
		Gruen();
		~Gruen();

		Farbe* wechseln();
		void anzeigen();
};

