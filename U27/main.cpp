#include <iostream>
#include "Ampel.h"
#include "Farbe.h"
#include "Rot.h"


using namespace std;

void main() {
	Ampel* a = new Ampel();

	for (int i = 0; i < 10; i++) {
		a->umschalten();
		a->getAnzeige()->anzeigen();
	}

	system("PAUSE");
}