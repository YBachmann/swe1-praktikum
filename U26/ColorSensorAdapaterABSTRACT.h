#pragma once
#include "ColorObject.h"

class ColorSensorAdapaterABSTRACT
{
public:
	ColorSensorAdapaterABSTRACT();
	~ColorSensorAdapaterABSTRACT();
	virtual void getColor(ColorObject *) = 0;
};

