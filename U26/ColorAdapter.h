#pragma once
#include "ColorSensorAdapaterABSTRACT.h"

class ColorAdapter : public ColorSensorAdapaterABSTRACT
{
public:
	ColorAdapter();
	~ColorAdapter();

	void getColor(ColorObject *);
};

