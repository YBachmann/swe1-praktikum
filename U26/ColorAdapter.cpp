#include <iostream>
#include "ColorAdapter.h"
#include "Drucker.h"

ColorAdapter::ColorAdapter()
{}

void ColorAdapter::getColor(ColorObject * Color)
{
	ColorObject * Farbe = Color;

	std::cout << "R: " << Farbe->getR() << std::endl;
	std::cout << "G: " << Farbe->getG() << std::endl;
	std::cout << "B: " << Farbe->getB() << std::endl << std::endl;


	//Berechnung der CMY Farbwerte
	int c = (100 - (Farbe->getR() * 100 / 255));
	int m = (100 - (Farbe->getG() * 100 / 255));
	int y = (100 - (Farbe->getB() * 100 / 255));


	Drucker printer;
	printer.print(c, m, y);
}

ColorAdapter::~ColorAdapter()
{}
