#include "ColorObject.h"
#include <stdlib.h>
#include <time.h>


ColorObject::ColorObject()
{
	srand(time(NULL));

	R = (int)rand() % 255;
	G = (int)rand() % 255;
	B = (int)rand() % 255;	
}

ColorObject::~ColorObject()
{}

int ColorObject::getR()
{
	return R;
}

int ColorObject::getG()
{
	return G;
}

int ColorObject::getB()
{
	return B;
}




