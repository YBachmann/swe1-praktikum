#pragma once

class ColorObject
{
public:
	ColorObject();
	~ColorObject();

	int getR();
	int getG();
	int getB();

private:
	int R, G, B;
};

