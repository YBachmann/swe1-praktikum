#pragma once
#include "ColorAdapter.h"
#include "ColorObject.h"

class ColorSensor
{
public:
	ColorSensor();
	~ColorSensor();

	void getColor();
};

